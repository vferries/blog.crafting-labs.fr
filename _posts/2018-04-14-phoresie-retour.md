---
layout: post
title: "Phorésie, un premier bilan"
lang: fr
date: 2018-04-16 15:00:00 +0100
categories: 
- "code"
- "apprendre"
- "partager"
---

[![By Albert kok [CC BY SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0/deed.en)], via Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Lemonshark.jpg/320px-Lemonshark.jpg)](https://upload.wikimedia.org/wikipedia/commons/9/93/Lemonshark.jpg){: .left-image}

Quand j'écris ce billet, les quatre premières sessions de [phorésie](/2018/01/16/phoresie/) ont eu lieu, une cinquième est planifiée.
Ça me semble un bon moment pour faire un premier point sur ce format.


# De quoi on a parlé

Quand j'ai lancé l'idée[^1], je ne savais pas trop où ça allait.
À vrai dire, je ne sais toujours pas, car aucune des sessions n'a été similaire. 
On a abordé des sujets aussi variés que les principes de bases de git, l'architecture hexagonale, les tests unitaires, d'autres plus généraux comme les IDE, les déploiements jusqu'à des choses qui peuvent sembler n'avoir rien à voir[^3] comme bienveillance versus bientraitance ou l'humanisme (colonialiste) de certains 'coach'. 
Et bien d'autres choses.

Je ne m'attendais pas à ça. J'avais imaginé qu'on se concentrerait sur des choses plus pratiques, plus proche du code. 
Je n'avais pas réalisé à quel point la discussion sur la pertinence des choix, l'architecture, ou placer les responsabilités semble ne jamais avoir lieu dans biens des contextes.

# Comment ça s'est passé

Si les thèmes étaient diversifiés, le déroulement lui a été le même depuis la première session : après une courte présentation du code, je propose plusieurs fonctionnalités à rajouter ou refactoring à faire et on y va.

Commence alors le développement, c'est moi qui m'y colle. Initialement, je pensais que l'on alternerait chacun notre tour au clavier. Au final, j'ai trouvé plus simple de garder le clavier : l'intérêt est plus dans la conversation que dans l'écriture du code.

Ensuite, je fais comme je ferai si j'étais seul à l'exception que je verbalise à voix haute toutes les questions que je me pose, les choix possibles, les raisons ou l'intuition que me font aller dans une direction ou dans une autre.
Et comme j'ai alors la chance d'avoir un.e binôme, je discute évidement avec elle.lui de tout ça : ce qui est d'habitude un monologue interne devient une conversation.
Quand il y a besoin, on prend le temps de détailler un concept, un outil ou une technique.

Je pense que rendre visible ce questionnement habituellement interne est ce qui m'intéresse le plus dans ce format.
C'est la partie qu'on ne voit jamais dans les formations ou les conférences.

# La suite

La suite c'est de continuer :) 

Cela dit, je me pose la question de la répétabilité : initialement, je pensais que c'est une session qu'une personne pourrait faire régulièrement, à quelques semaines ou mois d'intervalle. Aujourd'hui, j'ai un léger doute qu'une seconde fois soit aussi agréable et efficace. Il faudra sûrement trouver des variations pour que cela reste intéressant. Pour les participants comme pour moi.

En parallèle des sessions individuelles, j'ai envie d'expérimenter avec une version en petit groupe, 4 à 6 personnes.
Je prépare un billet pour décrire ce que j'imagine. Mais si l'idée vous appâte déjà, n'hésitez pas à me [contacter](/contact/).


-----
-- Image : [Lemonshark](https://fr.wikipedia.org/wiki/Phor%C3%A9sie#/media/File:Lemonshark.jpg) par Albert kok [CC BY SA 3.0 ](https://creativecommons.org/licenses/by-sa/3.0/deed.en), via Wikimedia Commons


[^1]: fortement influencé par mon camarade Stéphane Langlois. Merci
[^2]: java et Intellij
[^3]: alors qu'en fait si. Carrément. C'est même central.