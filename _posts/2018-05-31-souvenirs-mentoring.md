---
layout: post
title: "Souvenirs et mentoring"
date: 2018-05-30 10:11:00 +0100
categories: 
- "mentor"
- "software craftsmanship"
- "stage"
- "start-up"
---

[![By Daniel Hjalmarsson](/assets/images/daniel-hjalmarsson-269424-320px.jpg)](https://unsplash.com/photos/ljYfRMkNZJ0){: .left-image}

## ma première équipe

Ma première vraie[^1] expérience en tant que développeur c'était en 2001. J'étais en maitrise, officiellement c'était un stage de 3 mois, en pratique un CDD de 6.
C'était une start-up à Orléans qui venait de compléter une grosse levée de fond[^2] quelques semaines plus tôt.



Je ne sais pas si j'ai eu de la chance ou si c'était la norme à l'époque : je rejoignais une équipe de six[^3] mixte aussi bien en genre (3 femmes, 3 hommes) qu'en expériences et en compétences.

Je débarquais là avec plein de doutes et au moins autant de certitudes. Heureusement, il y avait toujours quelqu'un pour m'aider à lever mes doutes ou broyer mes certitudes.
Il ne s'agissait pas pour eux de faire mon travail, mais de me filer les références qui pouvaient me manquer, de suggérer des approches, d'expliquer une idée.
On discutait beaucoup. De problèmes techniques ou fonctionnels concrets mais aussi plus généralement sur notre façon de travailler.

Je me souviens par exemple de Sylvain, le dba[^4], qui après avoir lu un article, nous avait proposé d'organiser notre travail par priorité et de faire un point toutes les semaines où chacun⋅e expliquerait ses intentions pour la semaine et les problèmes qu'il⋅elle rencontrait[^9].

J'ai appris des tonnes de trucs que je n'aurais probablement jamais imaginés si j'avais été tout seul : l'intérêt du code compréhensible, l'utilité de rendre son application testable[^10], les problèmes liés à l'optimisation prématurée et le syndrôme NIH[^11], l'organisation de son travail, l'intérêt de communiquer et tellement d'autres choses.


## puis dans la recherche

Après ça, j'ai fait un dea puis un doctorat.
Le travail de recherche est différent. Même s'il s'inscrit dans un ensemble, le travail est plus individuel, personnel.

Mais la progression a été un peu la même : je suis arrivé sans savoir grand chose mais avec le soutien de ceux qui ont plus d'expérience, du thésard⋅e à quelques jours de la soutenance au directeur⋅rice de recherche renommé⋅e, toujours disponible pour éclairer une situation, découvrir des portes dans ce qui semblait être une voie sans issue.

C'est un cycle. Sans avoir rien changé, quelques années plus tard, on se rend compte que non seulement on est celui⋅celle vers qui les débutant⋅e⋅s se tournent quand il⋅elle⋅s sont bloqué⋅e⋅s, mais qu'en plus on est capable des les aider. On reproduit les schémas qui nous avaient réussi.

## et aujourd'hui

Il s'est passé un paquet de choses depuis. J'ai bossé avec un tas de gens, dans plein de contextes. Certains épanouissants, d'autres beaucoup plus toxiques.
Mais quand je me retourne sur le chemin réalisé je crois que les deux premières étapes significatives[^16] ont fortement influencé toute la suite.

Depuis quelques années, j'interviens régulièrement dans des start-ups, qui ont quelques années et qui me demandent de les aider à améliorer leur façon de faire du logiciel.
Et de plus en plus, j'ai le sentiment d'arriver trop tard.

Pas trop tard parce que le mal est fait, que le code est dans un état irrécupérable.
Même si la tentation de tout jeter et de recommencer à zéro est parfois grande, je doute que ce soit une solution[^8].

Je suis convaincu, parce que je l'ai vécu, qu'il n'est jamais trop tard pour rendre un code vivable tout en continuant à le faire évoluer (hint : ça demande du temps, du travail et un petit paquet de compromis :).

### trop tard pour quoi ?

Trop tard pour les développeur⋅euse⋅s eux mêmes.

Ces boites qui m'appellent on souvent un profil similaire : 2 ou 3 ans d'existence, des fondateur⋅rice⋅s qui n'ont pas ou peu d'expérience dans le développement et pas ou peu de moyens financiers. Alors la première version du logiciel est développée par un⋅e ou deux stagiaires/apprenti⋅e⋅s parce que c'est pas cher.

Des débutant⋅e⋅s sans personne avec de l'expérience dans leur metier à proximité. Personne pour lever leurs doutes et écraser leurs certitudes. Personne pour démarrer un cycle d'apprentissage vertueux avec eux.

Et quand enfin, la boite à les finances pour le faire, c'est trop tard : les apprenti⋅e⋅s devenu⋅e⋅s developpeur⋅euse⋅s ont grandi sans jamais avoir à se remettre en questions, ou sans être guidé⋅e pour le faire correctement. 
Alors que le leur startup vient de faire une grosse rentrée d'argent, il est peu probable que ça arrive. D'autant plus qu'il⋅elle⋅s ont contribué à une levée de fonds importante : difficile de changer des habitudes quand on a plusieurs millions de raisons de se convaincre qu'elles n'étaient pas si mauvaises.

Plus tard il⋅elle⋅s iront rejoindre d'autres sociétés, et reproduiront les schémas dans lesquels il⋅elle⋅s ont grandi. Et c'est le démarage d'un autre cycle, vicieux cette fois.

## on fait quoi ?

Je me suis longtemps posé la question.
Aujourd'hui, je comprends qu'une société qui démarre, voir qui n'a pas encore démarré, pense ne pas pouvoir se permettre d'intégrer une personne expérimentée dans leur équipe.
À plein temps, elle a probablement raison.

Comme je n'aime pas travailler à plein temps pour une société, ça m'arrange presque :)

L'idée serait alors un mentoring à distance avec deux actions principales pour commencer. Imaginons que je travaille avec une apprentie développeuse :

* une fois par semaine, en visio[^13], on discute de ce qu'elle est entrain de faire, ses objectifs et de la façon qu'elle envisage pour les atteindre, des problèmes qu'elle rencontre et des solutions qu'elle a trouvées. Mais aussi plus généralement du métier, des techniques qui vont avec. Quand c'est nécessaire on creuse plus en profondeur.
* le reste du temps, elle peut me contacter via messagerie[^14] quand il y a besoin.

À noter que ce format n'est pas fixe, il s'agit d'un point de départ qui évoluera en fonction des besoins et de la façon de travailler de chacun[^15].
Il est aussi fort probable que je jette des coups d'oeil dans la base de code pour alimenter nos conversations hebdomadaire.

### ça couterait combien ?

J'imagine entre 500€ et 1000€HT par mois, peut-être dégressif selon le nombre de personnes accompagnées.

Je n'ai pas d'idée arrêtée, si ça vous tente, on en discute.

### contact

Vous pouvez me contacter via le [formulaire de contact](/contact), sur [twitter @avernois](https://twitter.com/avernois) (mes dms sont ouverts) ou directement par e-mail [hello@crafting-labs.com](mailto:hello@crafting-labs.com)


---
Photo by [Daniel Hjalmarsson](https://unsplash.com/photos/ljYfRMkNZJ0) on Unsplash.

Merci à [Ludo](https://twitter.com/ludopradel) et [Vincent](https://gitlab.com/vferries) pour les relectures.

[^1]: j'avais fait plein de trucs avant, mais là, j'avais un contrat et un salaire décent pour l'accompagner :)
[^2]: j'ai 60 millions de francs en tête, un peu plus de 10 millions d'euros d'aujourd'hui 
[^3]: il y avait aussi 2 chefs d'équipe, les developpeurs de la version de départ il me semble, mais je n'ai pas vraiment de souvenir d'eux.
[^4]: administrateur de base de données
[^5]: mais je n'oublie pas Olwen, Marie-Do, Wilfried et Marie Laure (j'ai un doute sur ce dernier prénom).
[^6]: Il me semble que c'était les développeurs de la solution d'origine catapultés chefs en guise promotion. Probablement sans savoir ce que cela impliquait.
[^7]: je garde l'étape entre les deux et sa conclusion pour un autre billet ou autour d'un verre dans un bar :)
[^8]: pour tout un tas de raison qui mériterait un autre billet. Peut-être un jour.
[^9]: yep, c'était en 2001.
[^10]: les tests unitaires, pour moi, viendront plus tard, mais la reproductibilité en environnement controllé était un premier pas.
[^11]: écrire sa propre librairie pour gérer des sockets tcp n'était pas la meilleure idée du monde :). Pas plus qu'un serveur http.
[^12]: coucou LN :)
[^13]: ou en personne si on se trouve dans la même ville ce jour là.
[^14]: plutôt instantanné possiblement asynchrone genre slack/irc
[^15]: par exemple, pour certain⋅e⋅s, le point d'une heure par semaine pourrait se transformer en 10 min par jour. On peut envisager tout un tas de variations.
[^16]: dont je parle dans ce billet