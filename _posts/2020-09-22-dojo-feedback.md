---
layout: post
title: "Coding dojo - retour sur une expérience"
date: 2020-09-22 10:00:00 +0200
---

[![group of martial artists sitting on the ground by Thao Le Hoang](/assets/images/thao-le-hoang-yYSY93clr4w-320px.jpg)](https://unsplash.com/photos/yYSY93clr4w){: .left-image}
Fin mai, j'ai lancé l'idée d'un [coding dojo régulier]({% post_url 2020-05-27-coding_dojo_regulier %}). Pour une expérience qui allait s'étaler sur l'été et annoncée à l'arrache, j'ai été agréablement surpris par la réponse.

Après 12 semaines, le dojo prend une pause. J'en profite pour faire un bilan sur ce qui s'est passé et définir la forme de la suite.


<br>

# ce qui c'est passé

## quelques fais

La première session a eu lieu le vendredi 12 juin, 14h avec 5 participant⋅e⋅s, plus moi. Deux semaines plus tard un deuxième groupe démarrait le mercredi matin avec deux autres personnes.

La dernière session a eu lieu le vendredi 28 août. Au total, c'est 22 sessions, 12 pour les groupes du vendredi, 10 pour celui du mercredi qui a démarré plus tard.

Après quelques sessions, j'ai demandé aux participant⋅e⋅s de répondre individuellement deux choses : 
* s'il⋅elle souhaitait continuer ?
* combien il⋅elle souhaitait payer ? J'avais écrit [une page pour les aider à choisir un montant](https://gitlab.com/avernois/blog.crafting-labs.fr/-/blob/75860710b999536d5bc55853b2d191cc052b0f12/dojo/prix-libre.md)[^lienprix].

À la première question, une seule personne a choisi d'arrêter pour soulager son emploi du temps déjà trop complet.

À la seconde, tout le monde a souhaité payer quelques choses mais seulement 6 ont trouvé les moyens de le faire. Au total, j'ai facturé 3525 € HT sur l'ensemble des 7 participant⋅e⋅s, avec un minimum de 0€ et un max de 1000€ HT. Tout⋅e⋅s ceux⋅celles qui ont pu payer ont payé le tarif conseillé ou plus.


## les sessions

La structure est assez simple : quelques minutes pour expliquer le problème auxquels nous allons nous attaquer, puis une résolution collective. Pour finir, une clôture permet à chacun⋅e d'exprimer son ressenti sur la session.

Selon le nombre de participant⋅e⋅s, la partie résolution à pu prendre deux formes :
* \>= 3 personnes : les participant⋅e⋅s prennent le clavier tour à tour et s'occupe alors principalement de traduire en code la conversation des autres.
* <= 2 personnes : je m'occupe d'écrire le code permettant aux participant⋅e⋅s de se concentrer exclusivement sur la réflexion et l'élaboration d'une solution au problème courant.

Dans les deux cas, mon principal rôle est de faciliter la conversation et expliquer comment j'aborderai la situation.

Toutes les sessions ont utilisées le même problème comme support, le [kata fizzbuzz](https://codingdojo.org/kata/FizzBuzz/). Yep, 12 sessions sur le même katas :)
Histoire de briser la monotonie et pousser un peu dans d'autres directions, j'ai proposé des variations, autant dans l'espace du problème[^probleme] que dans celui des solutions[^solution].

# ce que j'en pense

Quand j'ai commencé, je ne savais pas trop ce que ça allait donner. J'ai d'ailleurs été explicite dès les premières sessions que c'était une expérience et que la peinture n'était pas vraiment sèche.
L'objectif était, et reste, de monter un (ou plusieurs) groupe stable et répéter le même kata encore et encore, pour aller en profondeur en construisant petit à petit sur ce qu'on a fait précédemment (tout en redémarrant de zéro à chaque fois :).

La répétition du katas était ma plus grande inquiétude. Je suis convaincu, pour le faire moi même, que c'est efficace pour progresser, j'avais un doute sur ma capacité à le faire comprendre au groupe. Mais, une fois passées les premières réticences, tout le monde a vu l'intérêt de l'approche. Et ça c'est cool.

Du côté d'organisation, j'ai pu constater que 4 ou 5 c'est trop pour que tout⋅e⋅s les participant⋅e⋅s restent engagé⋅e⋅s activement dans la conversation. Pour la suite, je vise des groupes de 3 : à 3, je peux gérer le clavier et organiser la conversation et la réflexion où tout le monde contribue.

Les groupes se sont constituées naturellement, avec parfois des écarts assez importants en termes d'expérience et background. Mais je suis ravi avec comment ça c'est passé et donc je range dans mes cartons mes idées pour constituer des groupes, le hasard marche bien :)

Il n'y a qu'un truc qui continue a me déranger dans la constitution des groupes, c'est qu'ils sont exclusivement masculins. C'est un truc qui pourrait me faire tout arrêter parce que ce n'est pas représentatif ni de la société dans laquelle nous vivons ni de la diversité dont on a besoin dans le développement logiciel. Je n'ai pas encore les clés, c'est clairement un aspect que je dois travailler pour être plus inclusif.


Un autre inconnu de cette expérience était l'aspect financier. Là encore, j'ai été agréablement surpris. 3525€ ça fait environ 160€ par sessions, c'est au-dessus de la barre des 150€ que je m'étais fixé pour continuer sans me poser de questions.
Cela dit, 2 sessions sur 2 jours différents pose beaucoup de contraintes sur mon emploi du temps : le plus souvent j'interviens en dev ou conseil sur des journées complète, du coup deux demie journée au dojo, c'est en fait 2 journées de bloquées, et 2 jours sur 5 à 150€ ce n'est pas viable sur le long terme.
Pour que gérer deux groupes soit viable, j'envisage de les ramener sur le même jour et ouvrir le mercredi matin si les 2 sessions du vendredi sont full.

# ce qui va suivre

La suite, c'est le redémarrage du dojo la première semaine d'octobre, pour 10 semaines qui nous mènerons jusqu'à la période de fin d'année.
A minima, il y aura un groupe le vendredi matin, probablement un autre l'après midi. Et un groupe le mercredi matin si ça semble viable (si les groupes du vendredi sont pleins et/ou il y a assez de participant⋅e⋅s pour faire 2 groupes le mercredi).

Ce sera des sessions de 90 min avec trois personnes.

Toujours à prix libre. Les prix conseillés refléteront ce passage de 4 à 3 personnes.

Si ça vous intéresse, il suffit de m'envoyer un petit message, par [twitter @avernois](https://twitter.com/avernois) (dm ouvert) ou via le [formulaire de contact](/contact).


Je vais écrire une page dédiée dans les prochaines heures/jours pour décrire précisement c'est qu'est le dojo et la forme qu'il prendra pour la prochaine période. (rien d'inovant par rapport à ce qui est déjà écrit dans cet article).

---
Crédits photos :

* group of martial artists sitting on the ground by [Thao Le Hoang on Unsplash](https://unsplash.com/photos/yYSY93clr4w)


[^probleme]: comme de nouvelle règle fonctionnelle, une reformulation du problème
[^solution]: des contraintes sur le code résultant
[^lienprix]: le lien est vers le fichier source tel qu'il était à l'époque où ce billet a été écrit. La page ayant changé depuis.