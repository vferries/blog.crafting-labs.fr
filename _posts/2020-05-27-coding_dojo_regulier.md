---
layout: post
title: "Un dojo pour le code"
date: 2020-05-27 10:00:00 +0200
categories: 
- "formations"
- "coding dojo"
---
[![group of martial artists sitting on the ground by Thao Le Hoang](/assets/images/thao-le-hoang-yYSY93clr4w-320px.jpg)](https://unsplash.com/photos/yYSY93clr4w){: .left-image}

> Le dojo (道場, dōjō?, [doːdʑoː]) [...]. Littéralement en japonais, dō signifie la voie (c'est le même caractère que le tao chinois), le dōjō est le lieu où l'on étudie/cherche la voie.. -- [Wikipédia](https://fr.wikipedia.org/wiki/Dojo)

La voie du code, ça fait un peu présomptueux. En même temps, apprendre à écrire du code maintenable est une quête sans fin, Je ne crois pas qu'il y ait un moment où l'on puisse se dire qu'on est arrivé au bout de la route et qu'il n'y a plus rien à faire pour progresser.

Ça fait longtemps que je tourne autour de l'idée de créer un espace dédié à l'apprentissage et la progression des compétences utiles ou nécessaires pour être développeu⋅r⋅se.
Au départ, j'imaginais un lieu physique, mais vu le contexte actuel, l'approche en ligne semble plus appropriée.

Et c'est pas plus mal, parce que c'est plus facile pour démarrer. Alors c'est parti ! ;)



# C'est quoi l'idée ?

Je le disais dans mon [précédent billet]({% link _posts/2020-02-12-formations.md %}), un des reproches que je fais aux formations de développement *classiques* c'est leur absence de suivi : elles ont lieu sur 1 à 3 jours et après c'est fini.

J'aimerai créer un espace où l'on pourrait apprendre et progresser sur la durée.

Un peu comme un dojo où l'on apprendrait le judo ou l'aikido, l'idée est de proposer des sessions courtes, 90 min[^session], régulières, toutes les semaines à heure fixe, en groupe fixe.

Je pense que la régularité est l'une des clés pour progresser, autant dans la répétition que la constitution des groupes.

En plus de ça, le dojo pourrait aussi être un lieu, en ligne, où les membres peuvent venir discuter/échanger autour de leur pratique en dehors des sessions.

# La proposition

## des sessions régulières

[![three persons pointing at a silver laptop by John Schnobrich](/assets/images/john-schnobrich-2FPjlAyMQTA-320px.jpg)](https://unsplash.com/photos/2FPjlAyMQTA){: .right-image}

Je pense que la plupart des sessions auront un format assez classique : en petit groupe[^groupe], on prend un sujet prétexte et on le résout collectivement.

Un peu à l'image des [coderetreat](https://www.coderetreat.org/pages/facilitating/structure-of-a-coderetreat/), on utilisera souvent le même problème parfois en introduisant une variation dans le sujet ou l'espace des solutions.
L'objectif de la répétition ici est d'explorer le sujet, voir les impacts que de petites variations peuvent avoir, tester des idées, des approches[^explorer], ...

Le format même de la session sera sujet à variation de temps en temps.

On partira sur des sessions de 90 min.
Elles se concluront par une clôture où les participant⋅e⋅s pourront revenir sur les moments clés de la session et faire des propositions pour les suivantes.

## des sujets variés

Même si certaines sessions pourront être orientées par un aspect spécifique, on abordera à chaque fois tout un tas de sujet : l'écriture du code, bien sur, mais aussi le design, l'architecture, les tests, la compréhension du problème à résoudre, la lecture de code existant, la recherche de défaut ou encore la communication avec ses collègues dev et toutes les personnes qui interviennent dans la création du logiciel. Liste non exhaustive :)

L'approche est d'aborder les sujets qui sont nécessaires aux participant·e·s pour continuer leur progression, il n'y a pas de limite ni de tabou[^tabou].

## un espace pour échanger

[![person using laptop by John Schnobrich](/assets/images/john-schnobrich-FlPc9_VocJ4-320px.jpg)](https://unsplash.com/photos/FlPc9_VocJ4){: .left-image}
En plus des sessions régulières, j'imagine un espace où les participant·e·s pourront échanger les un·e·s avec les autres et avec moi et les autres anima·teur·trice·s[^autres].

Cela aura probablement plusieurs formes : 
* des salons textuels pour échanger de façon synchrone et asynchrone
* une base de connaissances pour recenser les réponses aux questions qui sont posées régulièrement ou celle qui mériterait qu'on se les pose plus souvent :)

L'organisation globale est encore un peu flou, ça prendra forme à l'usage, comme le reste.

## cela s'adresse à qui ?

Le dojo est pour toutes les personnes qui souhaitent progresser dans leur pratique du développement logiciel. Il n'y a pas d'autres prérequis.

## ça aurait lieu quand ?

Dans ma vision des choses, progresser dans son métier est une partie intégrante du travail, par conséquent cela devrait avoir lieu pendant votre temps de travail.
Du coup, mes créneaux de prédilection seront en journée en semaine.

Cela dit, je n'ai pas envie de fermer la porte à c⋅eux⋅elles dont l'entreprise ne soutiendrait pas l'initiative, et on peut imaginer des créneaux à d'autres heures : tôt le matin, le midi, le soir, le we. Tout est possible :)

## combien ça coûte ?

[![vintage adding machine by Alvaro Reyes](/assets/images/alvaro-reyes-MEldcHumbu8-320px.jpg)](https://unsplash.com/photos/MEldcHumbu8){: .right-image}
Là encore c'est un peu flou. D'un côté, permettre aux gens de progresser dans leur métier du développement logiciel, c'est mon travail et mon travail c'est ce sur quoi je compte pour payer mon loyer (et le reste :).
J'aimerai aussi pouvoir faire intervenir parfois d'autres personnes pour l'animation et les payer correctement pour cela.

D'un autre, j'aimerai que l'argent ne soit pas un facteur de sélection, ça l'est déjà pour beaucoup trop de formation.

Comme pour d'autres projets[^prixprojets], on va partir sur un prix presque libre.

Presque libre parce que j'imagine qu'il pourrait y avoir de seuils :
* c'est toi, personnellement qui va payer : prix libre, pas de minimum, conseillé 500€/an[^echeance].
* tu es indépendant⋅e⋅s, ou dans une petite structure : prix libre, minimum à 1000€ HT/an, conseillé 1500€ HT/an[^echeance].
* ta société verse des dividendes à ses investisseurs, ou ta start-up a fait une grosse levée de fond : prix libre, minimum à 3000€ HT/an, conseillé 5000€ HT/an[^echeance].

À noter que les participant⋅e⋅s auront accès exactement à la même chose indépendamment du prix qu'il⋅elle⋅s ont payé.

# Ça m'intéresse, je m'inscris où ?

J'imagine qu'un jour il y a aura un joli formulaire, une page dédiée pour ça. Pour l'instant, tu peux m'envoyer un petit message via ce [formulaire de contact](/contact/) ou sur twitter [@avernois](https://twitter.com/avernois) (mes dm sont ouverts) et on voit ensemble pour les détails.


---
Merci à [Ludo](https://twitter.com/ludopradel) pour ses précieuses remarques.

Crédits photos :

* group of martial artists sitting on the ground by [Thao Le Hoang on Unsplash](https://unsplash.com/photos/yYSY93clr4w)
* three persons pointing at a silver laptop by [John Schnobrich on Unsplash](https://unsplash.com/photos/2FPjlAyMQTA)
* what’s going on here by [John Schnobrich on Unsplash](https://unsplash.com/photos/FlPc9_VocJ4)
* machine by [Alvaro Reyes on Unsplash](https://unsplash.com/photos/MEldcHumbu8)

[^session]: 90 min, toutes les semaines, c'est la première idée, mais ces valeurs ne sont pas définitive
[^groupe]: 4 à 6 au delà de ça, je trouve la communication en ligne compliquée.
[^explorer]: cette idée de variation sur un thème, de lien entre les sessions est un truc qui me manque dans les coding dojo communautaires auxquels j'ai pu participer.
[^tabou]: du moins pas que je puisse imaginer pour l'instant, peut être qu'on en découvrira.
[^autres]: [Ludovic Pradel](https://twitter.com/ludopradel) m'a déjà signifié qu'il était partant \o/
[^prixprojets]: comme [Dégaine]({% link _posts/2019-04-06-degaine.md %}) ou [phorésie]({% link _posts/2018-06-18-phoresie-inter.md %})
[^echeance]: on peut imaginer que ce montant soit payé en plusieurs fois. Il n'y a pas vraiment de porte fermées :)