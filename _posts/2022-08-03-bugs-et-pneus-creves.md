---
layout: post
title: bugs et pneus crevés
date: 2022-08-03 14:00:00 +0200
---

[![Defect broken flat tire locked post delivery bicycle by Markus Spiske](/assets/images/markus-spiske-JDuL2DnNUW4-unsplash.320px.jpg)](https://unsplash.com/photos/JDuL2DnNUW4){: .left-image}
Peu importe nos pratiques pour l'éviter, il y a toujours un moment où le logiciel que l'on développe ne se comporte pas comme prévu au point que des utilisat⋅eur⋅rice⋅s se retrouvent bloqué⋅e⋅s.


Et dans la plupart des organisations, cela entraîne l'équipe de développement à interrompre ses activités, à minima modifier ses plans, pour produire au plus vite un correctif qui permettra de débloquer c⋅eux⋅elles qui le sont.
Et c'est une bonne chose, proposer une solution pour permettre à tout le monde de reprendre ses activités au plus vite.

Dans mon expérience, ces corrections rapides ne le sont pas uniquement dans le délai. Elles sont aussi rapides dans l'analyse du problème, la mise en place de la solution ou l'évaluation des conséquences. Elle débloque mais ce n'est pas forcément une solution pérenne.

C'est l'équivalent logiciel de la roue de secours quand on a un pneu crevé. C'est pratique et ça permet de reprendre son trajet au plus vite avec un minimum de perturbations sur les plans que l'on avait fait. 
[![red car by Tory Bishop](/assets/images/tory-bishop-QzH-UGjKN8g-unsplash.320px.jpg)](https://unsplash.com/photos/QzH-UGjKN8g){: .right-image}
Mais, c'est une solution de courte durée, on ne devrait pas rouler indéfiniment avec une roue de secours sous peine de créer de nouveaux problèmes : usure prématurée des autres pneus, consommation excessive, tenue de route diminuée, &hellip;.
La roue de secours nous permet de gérer l'urgence et de gérer le problème au calme, a notre rythme : prendre le temps de passer au garage pour faire réparer ou changer son pneu, remplacer la solution temporaire par une plus pérenne.
Cela offre aussi un temps pour se poser des questions sur l'origine du problème et se rendre compte qu'il faudrait penser à réparer ce nid de poule dans l'allée du garage.

Trop souvent, dans les équipes logicielles que je croise, après un correctif rapide, une roue de secours, on ne planifie pas le passage au garage pour régler le problème de façon durable. On traite le blocage avec un *quick fix* et on passe à autre chose.
[![Broken mazda bus by Glenov Brankovic](/assets/images/glenov-brankovic-UCZfvrXi3ug-unsplash.320px.jpg)](https://unsplash.com/photos/UCZfvrXi3ug){: .left-image}
Et quand le blocage revient - et il revient toujours, parce que c'est dans la nature même de ces correctifs de ne s'attaquer qu'à la partie gênante, mais pas à l'origine du problème - que ce soit le même ou une conséquence du fix d'origine, on se retrouve, à nouveau, à devoir trouver une solution dans l'urgence, sans prendre le temps de faire une analyse en profondeur parce qu'il faut débloquer les client⋅e⋅s au plus vite. 

[![person holding black and grey metal tool by Enis Yavuz](/assets/images/enis-yavuz-CsYaNzll_rA-unsplash.320px.jpg)](https://unsplash.com/photos/CsYaNzll_rA){: .right-image}Mais ce cycle, problème bloquant qui engendre une correction dans l'urgence qui engendre un problème bloquant qui engendre une correction dans l'urgence qui engendre [&hellip;], ne peut pas durer indéfiniment. Il arrive toujours un moment où on a épuisé le stock de solutions rapides[^stock].
À ce moment, on se retrouve avec des utilisat⋅eur⋅rice⋅s coincé⋅e⋅s et une équipe qui, pour les décoincé⋅e⋅s, devra tout interrompre pendant plusieurs mois, le temps de refondre le système et corriger le problème à la source. 
Des mois avec la pression d'utisat⋅eur⋅rice⋅s mécontent⋅e⋅s, probablement des conséquences financières, alors qu'il n'aurait peut-être fallu que quelques semaines, sans pression, à notre rythme, si on avait fait le travail après le premier correctif.


**tldr;** quand on crève un pneu, on utilise une roue de secours dans l'urgence et on planifie une visite au garage par la suite. Quand un bug entraîne un correctif fait dans l'urgence, il faut planifier du temps pour faire la correction proprement, au calme.


---
Crédits photos :

* Defect broken flat tire locked post delivery bicycle by [Markus Spiske on Unsplash](https://unsplash.com/photos/JDuL2DnNUW4)
* red car by [Tory Bishop on Unsplash](https://unsplash.com/photos/QzH-UGjKN8g)
* Broken mazda bus by [Glenov Brankovic on Unsplash](https://unsplash.com/photos/UCZfvrXi3ug)
* person holding black and grey metal tool by [Enis Yavuz on Unsplash](https://unsplash.com/photos/CsYaNzll_rA)


[^stock]: parce qu'il y a une limite au volume de RAM que l'on peut rajouter sur une machine :)