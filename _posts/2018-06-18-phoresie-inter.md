---
layout: post
title: "Phorésie en groupe - Toulouse - 19 juillet 2018"
date:  2018-06-18 12:14:00 +0200
categories: 
- "phorésie"
- "formation"
- "développement"
- "artisanat"
---


[![By Kevin Lee](/assets/images/kevin-lee-59372-320px.jpg)](https://unsplash.com/photos/5bROqazy-40){: .left-image}

### tldr;

Une session de phorésie en groupe le jeudi 19 juillet 2018 dans les locaux d'Étincelle à prix libre avec un minimum de 48 € TTC (40 # HT) pour couvrir les frais.

[Inscriptions](https://www.lepotcommun.fr/billet/dysnoof3).


## proposition

Il s'agit d'une journée en phorésie pour un groupe jusqu'à 6 personnes.

Ce qu'il va se passer concrètement : en début de journée, nous choisirons une fonctionnalité à développer dans une application sur laquelle je travaille.

Ensuite je développe cette fonctionnalité en vous racontant tout ce qui se passe dans ma tête, les choix que je fais, les stratégies pour les faire, les outils, principes, questions, compromis, ...
Je réponds à vos questions, et j'en provoque quelques unes.

Si nécessaire, on met le développement en pause pour rentrer dans le détail d'un concept, d'un outil, ...

Dans les sessions précédentes, on a abordé un grand nombre de sujets, par exemple :
* les principes de bases des tests unitaires,
* le TDD,
* l'architecture hexagonale,
* git,
* le déploiement continue,
* la sécurité,
* la communication,
* la revue de code,
* la bienveillance,
* et bien d'autres.

J'ai présenté le format dans d'autres articles : [phorésie](/2018/01/16/phoresie/), [phorésie en équipe](/2018/05/07/phoresie-equipe/).

## date et lieu

La journée aura lieu le jeudi 19 juillet 2018 à partir de 9h30. Elle se terminera vers 17h, 17h30.

Elle se déroulera dans les locaux d'Étincelle Coworking, 2 rue d'Austerlitz à Toulouse (à côté de la place Wilson).

## tarif

Le tarif se passe en deux temps :
* à l'inscription, une participation au frais de 48 € TTC (40 € HT) à l'inscription. Cela permet de couvrir les frais de salle, repas du midi et grignotage au long de la journée.
* après l'évènement, vous pourrez compléter d'un montant de votre choix, c'est ce qui fera ma rémunération. Pour décider en conscience du prix je propose [quelques pistes de réflexion un peu plus bas](#le-prix-conscient).

## inscription

[Inscriptions sur le pot commun](https://www.lepotcommun.fr/billet/dysnoof3).

## informations complémentaires

### le prix conscient

* Actuellement, il me faut environ 1500 € net par mois pour payer toutes mes besoins de bases (loyer, assurances, nourriture, ...). Avec les frais de fonctionnement, les impôts et cotisations sociales cela veut dire qu'il faut que je facture 3000 € HT pour atteindre ce montant.
* Lorsque je donne ce genre de sessions en entreprise, je facture autour de 1000 € HT (1200 € TTC), plus les frais de déplacement.
* Idéalement, je facture 4 à 5 jours par mois : le reste du temps je fais des trucs, des expériences pour découvrir et apprendre des choses dont je fais ensuite profiter ceux⋅celles avec qui je travaille/fais des trucs.
* Ce type de journée, formation inter entreprise, est régulièrement facturée autour de 300 € HT (360 € TTC) par personne.

### Questions & Réponses

> C'est une formation ?

Mon intention est que tu apprennes des trucs qui t'intéressent/dont tu as besoin sur le développement logiciel. Par contre, je ne sais pas te dire ce que cela sera.

> J'ai besoin d'un prix fixe pour que ma société valide la prise en charge ?

Facile : choisis un prix qui te semble raisonnable (voir un peu plus :p) et voilà tu as un prix fixe. 
Pour t'aider à choisir un prix, tu peux regarder [les conseils](#le-prix-conscient).

Si tu as besoin d'un devis ou équivalent, [fais le moi savoir](/contact/) et je te fais ça.

> Est-ce que je peux ne venir qu'une demi-journée ?

Je privilégie les participant⋅e⋅s qui viennent pour la journée.
Si vraiment ça n'est pas possible pour toi, [fais le moi savoir](/contact/) : s'il reste des places disponibles quelques jours avant je te ferai signe.

> Est-ce qu'il y a un risque que ce soit annulé ?

À partir de deux participant⋅e⋅s, la journée aura lieu. S'il n'y en a qu'un⋅e, on verra ensemble de ce que l'on fait.

Si je devais annuler la journée, tu aurais le choix entre être remboursé ou reporter ton inscription à une prochaine session.

> On aura une facture ?

Oui. Elle sera éditée quand tu auras décidé du prix que tu payes. 
Elle inclura la participation au frais et le montant additionnel.

> Quel sont les moyens de paiement acceptés ?

Pour l'inscription, le moyen de prédilection est de passer par le site qui gère les inscriptions et de payer par carte bancaire.
Si ce n'est pas possible pour toi, [contacte moi](/contact/) et on trouvera un moyen

Pour le montant complémentaire :
* sur place par chèque ou monnaie (mais si tu peux éviter la monnaie, ça m'arrange),
* après : virement, carte bancaire et chèque.

> Toulouse c'est trop loin. C'est possible de faire ça près de chez moi ?

Bien sûr ! [Fais le moi savoir](/contact/) et on fera en sorte que ça arrive.

Remarque: cela peut avoir un impact sur la participation au frais qui devra couvrir mes frais de déplacement.

> Je me suis inscris mais en fait je ne peux pas venir ?

Le montant à l'inscription n'est pas remboursable. Mais il n'est pas perdu pour autant.
Tu peux choisir :
* d'en faire profiter quelqu'un d'autre
* de reporter ton inscription à une prochaine session (tu seras alors prioritaire pour t'y inscrire)

Remarque : si vraiment ce montant te fait défaut et son absence peut te placer dans une situation difficile des problèmes, [fais le moi savoir](/contact/), on trouvera une solution.

> J'ai des questions qui ne sont pas dans cette liste ?

[Envoie moi un message](/contact) et j'y répondrai au plus vite.

---
Photo by [Kevin Lee](https://unsplash.com/photos/5bROqazy-40) on Unsplash.