---
layout: page
title: Mentoring
permalink: /mentoring/
---

Une grande partie du métier de développeur⋅euse ne s'apprend pas sur les bancs de l'école mais sur le terrain, par la pratique et l'expérience.
Pour que cela se passe dans de bonnes conditions, il est souvent préférable d'être accompagné par des gens qui ont plus d'expériences qui peuvent indiquer les pièges ou les chemins plus sûrs.

Parfois, on se retrouve dans un environnement dans lequel ces personnes d'expériences ne sont pas là, pas disponibles, ou pas intéressées pour tenir ce rôle.

C'est là que j'interviens.

## Le principe

Il s'agit d'un accompagnement individuel pour développeur⋅euse.

Le point de départ pour initier la communication repose sur deux éléments :

* un rendez-vous hebdomadaire de 15 minutes à 1 heure au cours duquel on revient ensemble sur la semaine passée, les victoires, les obstacles
* une communication via messagerie asynchrone type slack pour communiquer le reste du temps

Le format évoluera en fonction de chacun et de ses besoins. Il pourrait alors inclure des sessions de binômage, de revue de code, coding dojo et autres.

## À qui ça s'adresse ?

En imaginant cette proposition j'avais en tête les stagiaires/apprenti⋅e⋅s qui trop souvent se retrouvent seul⋅e⋅s au démarrage des start-ups.
Après avoir discuté de l'idée avec quelques camarades, je pense que ça vaut le coup d'élargir :

> Les développeur⋅euse⋅s qui n'ont pas accès à une personne d'expérience dans leur contexte de travail.

* les stagiaires et apprenti⋅e⋅s sans developpeur⋅euse⋅s expérimenté⋅e⋅s dans l'équipe
* les developpeur⋅euse⋅s indépendant⋅e⋅s qui travaillent en solo ou changent souvent d'équipe
* les developpeur⋅euse⋅s dans des équipes où personne n'a le temps, l'envie, la patience ou la compétence pour les aider
* et ceux⋅celles qui trouvent l'idée intéressante :p

## Tarif

Le tarif de base est de *1000€* HT par mois. Il est dégressif en fonction du nombre de participant⋅e⋅s.

Cela dit, si l'idée t'intéresse vraiment mais que ton contexte ne te permet pas de payer ce prix, contacte moi quand même, particulièrement si tu fais partie d'une minorité ou d'un groupe sous-représenté dans le développement informatique[^1].

## Contexte

Dans l'article [Souvenirs et mentoring]({% post_url 2018-05-31-souvenirs-mentoring %}), je raconte des souvenirs des premières équipes dans lesquelles j'ai grandi et les prémices de cette proposition.

## Contact

On peut me joindre par [e-mail](mailto:hello@crafting-labs.com), via le [formulaire de contact](/contact/) ou sur twitter [@avernois](https://twitter.com/avernois) (mes dm sont ouverts).

---

[^1]: je pense entre autres aux femmes, personnes de couleur, transgenres ou en situation de handicap. Cette liste n'est pas exhaustive.