---
layout: page
title: "Coding dojo, combien ça coûte ?"
permalink: /dojo/prix-libre/
---

## Les prix conseillés, aka TLDR;

Les sessions s'organise autour d'une période d'environ 10 semaines.

Pour une période, le prix conseillé sont :
* c'est toi, personnellement qui va payer : prix libre, conseillé 250€ HT[^echeance]. 
* tu es indépendant⋅e⋅s, ou dans une petite structure avec peu de moyens : prix libre, minimum conseillé 500€ HT[^echeance].
* ta société verse des dividendes à ses investisseurs, tu es blindé de thunes ou ta start-up a fait une grosse levée de fond : prix libre, minimum conseillé 1000€ HT[^echeance].

---
---
## L'explication de ces montants.

### Le coût du temps et des choses

Actuellement, il me faut environ 1500 € net par mois pour payer tous mes besoins de bases (loyer, assurances, nourriture, ...). 
Cela correspond à peu près à 3000€ à facturer une fois déduit les frais fixe[^fixe], les impôts et contributions sociales.

Avec une base de 20 jours ouvrés par mois, cela signifie qu'en moyenne il faut que je facture au moins 150€ HT par jour.

Une session de 90 min avec les participant⋅e⋅s, une fois rajoutés les temps de préparation, communication, administration, ... c'est en fait, en moyenne, une demie journée de travail. Il faudrait qu'elle rapporte à minima 75€ HT.

Mon objectif, c'est des groupes de 3 personnes, donc 18,75€ HT par personne par session. C'est le montant que je retiens comme base pour la suite.

### minimum, moyenne, théorie, réalité

Ce montant de 18,75€ HT, correspond à un minimum théorique si je faisais ça 2 fois par jour 20 jours par mois, tous les mois. 
Ça n'inclut pas les vacances, les loisirs, les périodes où je n'ai pas de travail[^travail], les imprévus ... 
En règle générale, je multiplie par 4 pour avoir un matelas suffisant.

### Des différences de situations

De plus, l'argent n'a pas la même valeur pour tout le monde :
* si vous en avez beaucoup 75€ peut paraître ridicule, et une montagne inaccessible si vous en avez peu
* une entreprise passera cette dépense en frais donc pas d'impôt, et moins de contributions sociales sur ce montant, alors qu'un particulier ne pourra pas et sera chargé plein pot, en plus de la TVA.

Bref, demander le même montant à tout le monde, c'est un peu injuste. 
Du coup, des différences de prix selon le contexte parait une bonne solution pour palier à cela.
J'imagine trois grandes catégories :

* particulier : tu payes juste le minimum théorique, 18,75€ par session
* freelance/société avec peu de moyen : tu payes un peu plus 37,5€ par session
* freelance/société avec de l'argent : tu payes plein tarif 75€ par session

### Le prix libre pour équilibrer

Mais ce découpage reste encore à l'emporte pièce, 37,5€ peut être beaucoup pour une jeune développeuse qui démarre alors que ça ne poserait aucun soucis, financier ou moral, à une personne qui facture 1000€ par jour, 150 jours/an d'en payer 100€.

Comme je n'ai aucune envie d'éplucher votre comptabilité pour savoir combien il est juste de vous facturer, ça me parait sensé de laisser à chacun⋅e le soin de décider pour sa situation, d'où le prix libre avec des montants à titre indicatif.

À noter que si tout le monde décide de ne rien payer, où très peu, l'activité s'arrêtera. C'est là où j'espère qu'un équilibre se trouve : des gens qui ont les moyens payent beaucoup plus permettant ainsi à d'autres, moins aisé⋅e⋅s, de profiter des sessions.

Inversement, un budget plus élevé permettrait aussi de faire intervenir d'autres anima⋅teur⋅trice⋅s de temps à autres ou de financer d'autres activités liées au dojo sans objectif de rentabilité.

### La transparence 

Ce qu'une personne décidera de payer restera entre cette personne et moi. 
Je ne demanderai pas d'expliquer le montant[^explication] et tout le monde sera traité de la même façon indépendamment de la contribution.

Par contre, je publierai les stats, à minima le montant total, sûrement d'autres (moyenne ? écart type ? min/max ?). 
L'idée est que l'activité soit transparente autant sur les entrées que les sorties (ce que je garde pour moi, ce qui va à d'autres intervenant⋅e⋅s, ce que je garde pour financer d'autres activités liées au dojo, ...).




---
[^echeance]: on peut imaginer que ce montant soit payé en plusieurs fois. Il n'y a pas vraiment de portes fermées :)
[^fixe]: serveur, logiciel, assurances, ...
[^frais]: hors frais spécifique à l'activité facturée.
[^travail]: par exemple, au moment où j'écris cet article, fin juin 2020, en partie à cause de COVID 19, je n'ai quasiment rien facturé depuis fin avril.
[^36]: c'est le rythme scolaire 'traditionnel'.
[^explication]: mais je vous écouterai avec plaisir si vous avez envie/besoin de le faire.