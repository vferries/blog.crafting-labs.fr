---
layout: page
title: "Phorésie en groupe - Lyon - 25 avril 2019"
categories: 
- "phorésie"
- "formation"
- "développement"
- "artisanat"
noindex: true
---

[![By Kevin Lee](/assets/images/kevin-lee-59372-320px.jpg)](https://unsplash.com/photos/5bROqazy-40){: .left-image}

## le session

Cette session s'est tenue à la Cordée République à Villeurbanne.
Nous étions 5.

## le contenu

Nous avons implémenté la fonctionnalité ["Remove a product"](https://gitlab.com/avernois/pay_what_you_want-stripe/issues/5) du projet de [serveur de paiement à prix libre](https://gitlab.com/avernois/pay_what_you_want-stripe).

Il y avait deux activités dans cette fonctionnalité :
* réduire à zéro le stock du produit
* s'assurer que l'accès à cette fonctionnalité soit restreinte à une personne autorisée

Si la première partie a été assez triviale, la seconde a levé un grand nombre de questions sur la responsabilité de cette opération.

On a réalisé une implémentation dont l'idée de départ avait l'air intéressante : un `Authorizer` qui vérifierait la validité d'un token au niveau de la couche d'adaptation web <-> core et passerait son résultat à l'action concernée.

Je ne suis pas convaincu par le résultat qui permet à la couche d'adaptation de bypasser l'appel à l'authorizer (volontairement ou non). Cela donne une trop grande responsabilité à cet endroit que je préfère le plus ignorant possible (se contentant de faire de la transformation).
En conséquence, la [merge request](https://gitlab.com/avernois/pay_what_you_want-stripe/merge_requests/29) va être close.

Par contre, comme on avait commité régulièrement, j'ai pu facilement réimplémenter une autre version (basée sur un service) sans avoir à tout refaire dans le train du retour :) La [nouvelle merge request est là](https://gitlab.com/avernois/pay_what_you_want-stripe/merge_requests/30).

On a également entre aperçu un possible problème de concurrence entre l'appel à `productService.emptyStock` et le mécanisme de `hold`/`release`.

## les dépenses
### directes
Il s'agit des dépenses qui n'auraient pas eu lieu si cette journée n'avait pas été organisées.

Pour cette session, il n'y a que la salle:

* Salle Castor, La cordée république : 148,80€ TTC (124€ HT)

### indirectes
Il s'agit des dépenses qui auraient été faites même si la session n'avait pas eu lieu mais qui auraient empêché sont bon déroulement.

Pour cette session, c'est le trajet Toulouse <-> Lyon et l'hébergement à Lyon (si la session n'avait pas eu lieu je serais tout de même venu à Lyon pour l'unconference du 26/04/19).

* Train Toulouse <-> Lyon : 89 + 83 = 172€
* Hébergement AirBnB : 258 €

## prix libre

Le prix de la session est libre.

Si vous le souhaitez et/ou en avez la possibilité, vous pouvez compléter cette somme du montant de votre choix. C'est ce qui fera ma
rémunération pour la journée.

Pour vous aider un choisir un montant pertinent, vous pouvez prendre en compte les dépenses liées à la session ainsi que les informations plus générales que j'ai posées [sur la page d'une précédente session](/2018/06/18/phoresie-inter/#le-prix-conscient).

Une fois que vous avez décidé de votre montant, vous pouvez payer :
* par chèque ou virement : envoyez-moi [un message](/contact) avec le montant que vous souhaitez payer et je vous envoie la facture avec toutes les informations nécessaires.
* par carte bancaire : via [le serveur de paiement](https://pay-server.crafting-labs.fr/) (warning: l'interface est minimaliste :). Je vous enverrai la facture au plus vite ensuite.

Le mode préféré est le virement.


## feedback

Je suis preneur de tout retour sur cette session (format, contenu, déroulement,  repas, info avant/après, ...) que vous pourriez avoir envie de me faire.
Selon votre préférence, cela peut-être un message écrit ([dm/email](/contact)), une conversation vocale ou visio voir ouvrir une merge request sur [cette page sur gitlab](https://gitlab.com/avernois/blog.crafting-labs.fr/blob/published/{{page.path}}).

Cela m'aidera à savoir comment améliorer ce format, et savoir quoi en faire à l'avenir.