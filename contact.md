---
layout: page
title: Contact
permalink: /contact/
---

Vous pouvez me contacter :
* via [twitter @avernois](https://twitter.com/avernois) (mes dms sont ouverts),
* par e-mail [hello@crafting-labs.com](mailto:hello@crafting-labs.com)
* ou en utilisant le [formulaire de contact](#formulaire) ci-dessous.


### Formulaire

   <form action="//formspree.io/hello@crafting-labs.com" method="POST">
      <fieldset>
        <p class="field">
        	<label for="_subject">Sujet&nbsp;:</label>
        	<input name="_subject" id="_subject" type="text" size="30" maxlength="255" value="" />
        </p>
                
        <p class="field">
        	<label for="message">Message&nbsp;:</label>
        	<textarea name="message" id="message" cols="35" rows="7"></textarea>
        </p>

        <p class="field">
        	<label for="name">Nom ou pseudo&nbsp;:</label>
        	<input type="text" name="name" size="30" maxlength="255" value="" />
        </p>
        
        <p class="field">
        	<label for="_replyto">Adresse email (si vous voulez une réponse)&nbsp;:</label>
        	<input type="email" name="_replyto" size="30" maxlength="255" value="" />
        </p>
        
        <p class="field">
        	<label for="site">Site web (facultatif)&nbsp;:</label>
        	<input name="site" type="text" size="30" maxlength="255"
        value="" />
        </p>
        
        
        	<input type="text" name="_gotcha" style="display:none" />
        	<input type="hidden" name="_next" value="/merci/" />
        <p>
        	<input type="submit" value="Send">
        </p>
      </fieldset>
    </form>