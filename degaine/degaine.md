---
layout: page
title: Dégaine - Un compte à rebours à deux faces
permalink: /degaine/
---


![Dégaine - un compte à rebours à deux faces](/assets/images/degaine/DSC_8841.JPG){:.left-image}


## Fonctionnalités

## Caractéristiques


## Prix

## Acheter


## Questions Réponses

Si vous me suivez sur twitter, vous aurez constaté que depuis un an ou deux je publie des petits projets électronico-lumineux, généralement ayant une relation avec le temps.
Ça fait longtemps que ça me démange d'en faire une petite production à mettre en vente. Ce jour est venu.

Le projet s'appelle *dégaine*, c'est un compte à rebours qui a été pensé avec le suivi du temps durant une conférence ou un atelier.

Il est configurable de 0[^5] à 99 minutes. Une fois lancé, il indique le temps restant en minutes d'abord puis en secondes pour les 99 dernières secondes.
Lorsque 50% du temps est écoulé il change de couleur et change encore à 75%.

Une particularité est que le temps restant est lisible des deux cotés.

Je prévois les premières livraisons au plus tard début septembre 2018. 

Le prix est libre avec un prix conseillé à 90€ TTC et un minimum à 45€ TTC.

# prix (presque) libre

## un prix minimum

Le prix minimum est fixé à 45€ TTC (37,5€ HT). Il est basé sur deux composantes :

* le prix des matériaux :
J'estime à 15€ les matériaux.

    * 2 x 3€ de pcbs (1 pour l'afficheur, un pour la logique)
    * 2,5€ d'arduino
    * 2€ de leds
    * 3€ de bois
    * 1.5€ de connectique

* le temps nécessaire :
En moyenne, si je ne fais que ça, je pense pouvoir en fabriquer 4 à 5 par jours. Soit environ 130 par mois[^4].
Actuellement, pour payer mes dépenses personnelles[^2] j'ai besoin de 1500€ par mois. Pour réaliser ce bénéfice, grosso modo, il faut que mon entreprise facture le double[^3] soit 3000€. Et donc un prix hors matériaux de 22€

15 + 22 = 37€ HT.

J'arrondis à 45€ TTC. En dessous de ce prix, ce n'est pas viable[^6].

## un prix conseillé

Le prix conseillé, c'est le double: 90€ TTC (75€ HT).

Si je ne faisais que ça, à ce prix, je pourrais consacrer la moitié de mon temps à améliorer l'existant et imaginer d'autres produits.
Je pourrai même envisager de faire des pauses :)

## pourquoi ?

Parce que la valeur est une question compliquée.
Le prix c'est un mélange bizarre entre ce que ça coûte à produire, le bénéfice que l'on vise, ce qu'on imagine que l'utilisateur⋅rice est prêt⋅e à payer, et de la valeur qu'il ou elle accorde tant à l'objet qu'au travail qui a permis sa réalisation.

Si j'ai la main sur les deux premiers éléments (coût/bénéfice), les autres dépendent de vous. 
Le prix libre permet alors à chacun de contribuer selon ses moyens, son besoin ou son envie.

Ça pourrait aussi permettre à ceux⋅celles qui le souhaiteraient de mettre un peu plus et m'encourager à continuer ce type d'initiative.
S'il y a une limite basse, il n'y a pas de limite haute :p

Et puis, ça fait longtemps que je voulais tenter l'expérience du prix libre. Ça me semble une bonne occasion :)
Je pense que je vais tenter d'autres expériences autour de cette idée.

# livraisons début septembre ?

Et oui, c'est long. Il y a deux raisons principales à ce délai :

* le design final : aujourd'hui, j'ai des prototypes fonctionnels. Il reste encore du travail pour pouvoir les fabriquer de façon reproductible, principalement les pcbs.
* les composants : pour éviter une explosion dramatique du prix minimum, ils sont commandés directement en chine avec des délais de transit de 3 à 6 semaines.

Cela dit, votre attente ne sera pas aveugle. Je posterai au fur et à mesure des nouvelles de l'avancement sur twitter ou sur le blog.

Je profiterai aussi de ce temps pour rajouter des fonctionnalités, probablement autour de la configuration.


[^1]: les valeurs de 50% et 75% sont susceptible de changer en fonction des retours.
[^2]: loyer, électricité, nourriture, assurance, ...
[^3]: les 1500 restant partent en frais fixes de l'entreprise (serveurs, documentation, logiciel, téléphone, ...), cotisations sociales et autres impôts
[^4]: sur une base de 30 jours par mois
[^5]: et dans ce cas, il s'arrête immédiatement, ce qui n'est pas très utile :)
[^6]: en fait, même à ce prix, ça ne l'est pas vraiment. Les plus observateur⋅rice⋅s auront remarqué que je ne prends pas en compte le temps de développement ni les frais de port entre autres.