---
layout: page
title: Cancel
permalink: /degaine/cancel/
---

Il semblerait que vous ayez annulé votre achat lors du paiment.


Si c'est à la suite d'un problème rencontré ou si vous pensez qu'il s'agit d'une erreur, n'hésitez pas à me [contacter directement](/contact).

Pour retourner sur [la page de dégaine, c'est par là](/degaine/shop).