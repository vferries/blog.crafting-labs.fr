---
layout: page
title: Travaillons ensemble
permalink: /ensemble/
---

J'ai mis du temps à trouver un titre qui me plaise à cette page[^8].
"Travailler ensemble" correspond à ce que j'ai envie faire. Je serai ravi de travailler **avec** vous. Mais en aucun cas, je ne travaillerai **pour** vous.

La nuance est subtile, je le reconnais. J'ai envie de l'expliciter comme ça : si vous connaissez exactement l'endroit que vous souhaitez atteindre et le chemin pour y arriver, alors ne me contactez pas, on finirait malheureux tous les deux. 
Par contre, si vous avez des besoins et l'envie qu'on trouve ensemble une façon d'y répondre, alors vous êtes au bon endroit. Et pour me contacter, c'est [par là](#contact) :)

Pour aider à la lisibilité, j'ai divisé ce que nous pourrions faire ensemble en quatre groupes : [Apprendre, partager](#apprendre-partager) / [Accompagner, Conseiller](#accompagner-conseiller) / [Développer](#développer) / [Fabriquer](#fabriquer).
Mais ces frontières sont arbitraires, elles n'ont aucune réalité concrète et ces groupes se recoupent, s'entremêlent, se superposent joyeusement.

Cette liste est non-exhaustive : je suis ouvert à des idées que je n'aurais pas eue[^7]. 
Si vous avez une idée ou un besoin qui ne rentre pas dans les cases, ne vous retenez surtout pas [de me contacter](#contact).

## Apprendre, partager

Je ne me retrouve plus vraiment dans les formations 'classiques' : un.e sachant.e qui délivre son savoir à des apprenant.e.s dans un contexte très formel que je trouve souvent très/trop éloigné de la réalité du travail que l'on fait.

À la place, j'explore de nouvelles façons et d'autres formats basés sur la pratique partagée et la réflexion collective. Quelques formats possibles : 

### Phorésie

Au lieu de venir chez vous développer une application, vous me suivez une journée à développer une des miennes.

J'ai détaillé le concept plus amplement dans [ce billet](/2018/01/16/phoresie/).

* durée : 1 journée. répétable.
* audience : toute personne ayant envie d'écrire du code. 1 personne par session.

### Mob-programming

Un peu comme phorésie, mais dans l'autre sens. Et en groupe : votre code, un vidéo projecteur, une fonctionnalité à développer et on y va tous ensemble.

Tous les détails dans le billet dédié [Une formation les mains dedans](/2015/03/17/une-formation-les-mains-dedans).

* durée : 2 à 5 jours + 1 jour de préparation préalable[^3]
* audience : une équipe de développement[^2]. 6 personnes max.

### Mentoring

Une grande partie du métier de développeur⋅euse ne s'apprend pas sur les bancs de l'école mais sur le terrain, par la pratique et l'expérience.
Pour que cela se passe dans de bonnes conditions, il est souvent préférable d'être accompagné par des gens qui ont plus d'expériences qui peuvent indiquer les pièges ou les chemins plus sûrs.

Parfois, on se retrouve dans un environnement dans lequel ces personnes d'expériences ne sont pas là, pas disponibles, ou pas intéressées pour tenir ce rôle.

C'est là que j'interviens.

* durée : un point hebdomadaire et un support permanent (mais asynchrone).
* audience : développeur⋅euse ayant envie de progresser. Session individuelle.

Tous les détails sur la page dédiée [Mentoring](/mentoring).


### Code Retreat

Une journée pour s'ouvrir l'esprit, essayer des choses et prendre du recul sur ses pratiques quotidiennes de développement.

Voir le billet écrit à l'occasion du [Global Day of CodeRetreat 2012](/2011/11/12/code-retreat-toulouse-3-decembre/)

* durée : 1 journée
* audience : développeuse.eur.s. Entre 8 et 16 participant.e.s.


## Accompagner, conseiller

Dans ma vision, la nuance entre accompagnement et formation est vraiment très floue. Si elle existe, elle se trouve peut-être dans la différence entre _comprendre une pratique_ et _intégrer une pratique_ ie. faire en sorte que cette pratique devienne une part naturelle de votre façon de faire. 
Par exemple : comprendre les bases du TDD ou de l'agilité est assez simple. Rendre leur pratique fluide et naturelle requiert du temps et des efforts.

Ce temps, c'est à vous[^12] de le prendre. Ces efforts, c'est à vous[^12] de les faire. 

Là où je peux vous aider, c'est en vous suggérant[^13] des approches qui vous éviteront de perdre du temps et de l'énergie inutilement[^14], vous faire prendre du recul sur votre pratique et vous montrer quand il me semble que vous vous égarez de l'idée de départ.

note : si votre objectif est de mettre en place Scrum, ou TDD ou une autre méthode précise, alors je ne suis pas la bonne personne. L'objectif de mon travail d'accompagnement est de vous permettre de trouver la façon qui vous convient dans votre contexte[^15]. Les méthodes sont un moyen, éventuellement une étape, en aucun cas un objectif.

### Conseil à la demande

Des fois, on a juste besoin d'un regard extérieur pour mettre en perspective une situation, trouver d'autres angles pour aborder un sujet, débloquer un sujet.
Prenez un rendez vous d'une heure en ligne rapidement, et discutons-en.

Dans [binôme à la demande](/2018/03/08/binome_a_la_demande/) je décris le principe. Le billet est très orienté développement[^11], mais ce n'est pas une restriction du sujet.

C'est aussi une bonne entrée en matière si vous envisagez de me faire intervenir plus régulièrement dans votre contexte.

## Développer

Je suis avant tout un développeur[^6], un bâtisseur de logiciel. J'aime trouver une solution adaptée à des problèmes.
Si vous avez un problème à résoudre, je serai ravi qu'on essaie ensemble, progressivement, de construire une solution logicielle[^1].

note: si vous êtes convaincus d'avoir déjà la solution et/ou avez un agenda très précis à respecter, alors je ne suis pas la bonne personne.

## Fabriquer

J'aime fabriquer des choses qui ont une concrétisation physique, des objets que l'on peut toucher.
Depuis quelques temps ces objets prennent la forme de joyeux mélanges d'électronique, de LEDs, de bois et d'acrylique. Comme par exemple [Gouge](https://www.gitlab.com/avernois/gouge) ou [Rainbow Binary Clock](https://gitlab.com/avernois/rainbow-binary-clock)[^9].

Par exemple, on pourrait imaginer et réaliser ensemble un indicateur de build à l'image de votre équipe/produit/autre.

note: il ne s'agit pas de construire des produits à produire en masse. On parle d'objet unique ou micro série (max 10 ?) produit à la main[^10].

## Contact

On peut me joindre par e-mail via ce [formulaire de contact](/contact/) ou sur twitter [@avernois](https://twitter.com/avernois) (mes dm sont ouverts).

-----

-- Un grand merci à Ludovic et Anabelle pour les corrections

[^1]: ou pas.
[^2]: product owner, ops et testeuses.eurs font évidemment partie de l'équipe de développement.
[^3]: dont l'objectif est de me familiariser un peu avec le code et l'environnement de travail. C'est optionnel, mais fortement conseillé.
[^4]: ou deux, probablement pas plus de trois.
[^5]: sur au moins un an. Voir plus.
[^6]: et pourtant, c'est presque dernier groupe dont je parle ? trouverez vous pourquoi ?
[^7]: je me demande même si ce n'est pas celles que je préfère. Surprenez moi :)
[^8]: un énorme merci à [Ludo](https://twitter.com/ludopradel) qui m'a mis sur la voie.
[^9]: yep, je suis très branché horloge.
[^10]: en tout cas, c'est là où moi je m'arrêterai. Si après vous voulez prendre les design auxquels ont est arrivé et en faire des lots, libre à vous :)
[^11]: c'est de là que vient l'idée.
[^12]: et par vous, j'entends toute l'organisation impliquée
[^13]: de quelques jours par semaine à quelques jours par mois selon votre avancée.
[^14]: mais ça vous demandera toujours du temps et des efforts continus.